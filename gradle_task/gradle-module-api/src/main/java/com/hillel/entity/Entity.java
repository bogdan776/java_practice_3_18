package com.hillel.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public abstract class Entity implements Serializable {

    private int id;
}