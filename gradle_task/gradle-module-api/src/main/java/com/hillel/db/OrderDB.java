package com.hillel.db;

import com.hillel.entity.Order;

import java.util.List;

public interface OrderDB extends EntityDB<Order> {

    List<Order> getOrdersByUserId(int id);
}
