package com.hillel;

import com.hillel.entity.Order;
import com.hillel.entity.User;
import com.hillel.service.OrderService;
import com.hillel.service.UserService;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import java.util.List;

public class Main {
    private static final Logger LOG = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        UserService us = new UserService();
        OrderService os = new OrderService();

        LOG.info("========= LOG IN =========");
        User user = us.logIn("login1", "password1");
        if (user != null) {
            System.out.println(user);
            DateTime date = DateTime.now();
            String dateNow = date.toString("yyyy-MM-dd");
            os.addOrderToDB(user.getId(), dateNow, "testOrder");

            LOG.info("========= GET USER'S ORDERS =========");
            List<Order> orderList = os.getOrdersByUserId(user.getId());
            for (Order o : orderList) {
                System.out.println(o);
            }

            LOG.info("========= DELETE FIRST ORDER =========");
            os.delete(orderList.get(0).getId());

            orderList.clear();

            LOG.info("========= GET USER'S ORDERS AFTER DELETE =========");
            orderList = os.getOrdersByUserId(user.getId());
            orderList.forEach(System.out::println);
        }
    }
}