package com.hillel.impl;

import com.hillel.db.UserDB;
import com.hillel.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDBImpl implements UserDB {

    List<User> users;

    {
        users = new ArrayList<>();

        User user1 = new User();
        user1.setId(0);
        user1.setLogin("login1");
        user1.setPassword("password1");
        user1.setName("Name 1");
        user1.setPhoneNumber("7304050");

        User user2 = new User();
        user2.setId(1);
        user2.setLogin("login2");
        user2.setPassword("password2");
        user2.setName("Name 2");
        user2.setPhoneNumber("7305060");

        users.add(user1);
        users.add(user2);
    }

    @Override
    public User logIn(String login, String password) {
        List<User> usersLogin = users.stream().filter(l -> l.getLogin().equals(login)).collect(Collectors.toList());
        for (User u : usersLogin) {
            if (u.getPassword().equals(password)) return u;
        }
        return null;
    }

    @Override
    public void create(User user) {
        User newUser = user;
        newUser.setId(users.size());
        users.add(newUser);
    }

    @Override
    public List<User> getAll() {
        return new ArrayList<>(users);
    }

    @Override
    public User getById(int id) {
        return users.get(id);
    }

    @Override
    public void deleteById(int id) {
        users.remove(id);
    }
}
