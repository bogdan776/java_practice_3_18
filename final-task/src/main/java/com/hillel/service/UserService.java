package com.hillel.service;

import com.hillel.dto.UserDto;
import com.hillel.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserService extends UserDetailsService {

    boolean save(User user);

    UserDto findUserByUsername(String username);

    boolean updatePassword(String password1, String password2, User user);

    boolean updateUserInformation(String username, String firstName, String lastName, Integer age, User user);

    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
