package com.hillel.service.impl;

import com.hillel.dto.NoteDto;
import com.hillel.mapper.NoteMapper;
import com.hillel.model.Note;
import com.hillel.model.User;
import com.hillel.repository.NoteRepository;
import com.hillel.service.NoteService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class NoteServiceImpl implements NoteService {

    private final NoteRepository noteRepository;

    private final NoteMapper noteMapper;

    @Override
    public void save(Note note) {
        note.setDateOfCreation(new Date());
        note.setDateOfChange(new Date());
        noteRepository.save(note);
    }

    @Override
    public List<NoteDto> findAllByUser(User user) {
        List<NoteDto> notesDto = noteMapper.notesToNotesDto(noteRepository.findAllByUser(user));
        notesDto = notesDto.stream()
                .sorted((o1, o2) -> -o1.getDateOfChange().compareTo(o2.getDateOfChange()))
                .collect(Collectors.toList());
        return notesDto;
    }

    @Override
    public boolean update(Long id, String title, String text) {
        Note note = noteRepository.findById(id).orElse(null);
        if (note != null) {
            if (!title.equals(note.getTitle()))
                note.setTitle(title);
            if (!text.equals(note.getText()))
                note.setText(text);
            note.setDateOfChange(new Date());
            noteRepository.save(note);
            return true;
        } else return false;
    }

    @Override
    public void delete(Long id) {
        noteRepository.deleteById(id);
    }
}
