package com.hillel.service.impl;

import com.hillel.dto.UserDto;
import com.hillel.mapper.UserMapper;
import com.hillel.model.Role;
import com.hillel.model.User;
import com.hillel.repository.UserRepository;
import com.hillel.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;

@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserMapper userMapper;

    @Override
    public boolean save(User user) {
        User userFromDB = userRepository.findUserByUsername(user.getUsername());
        if (userFromDB == null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setRoles(Collections.singleton(Role.USER));
            user.setDateOfRegistration(new Date());
            user.setDateOfLastVisit(new Date());
            user.setActive(true);
            userRepository.save(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public UserDto findUserByUsername(String username) {
        return userMapper.userToUserDto(userRepository.findUserByUsername(username));
    }

    @Override
    public boolean updateUserInformation(String username, String firstName, String lastName, Integer age, User user) {
        boolean leastOneChangeFound = false;

        if (!user.getUsername().equals(username)) {
            User check = userRepository.findUserByUsername(username);
            if (check == null) {
                user.setUsername(username);
                leastOneChangeFound = true;
            } else {
                return false;
            }
        }

        if (!firstName.equals(user.getFirstName())) {
            user.setFirstName(firstName);
            leastOneChangeFound = true;
        }

        if (!lastName.equals(user.getLastName())) {
            user.setLastName(lastName);
            leastOneChangeFound = true;
        }

        if (!age.equals(user.getAge())) {
            user.setAge(age);
            leastOneChangeFound = true;
        }

        if (leastOneChangeFound) {
            userRepository.save(user);
        }
        return true;
    }

    @Override
    public boolean updatePassword(String password1, String password2, User user) {
        if (!password1.equals(user.getPassword())) {
            if (password1.equals(password2)) {
                user.setPassword(passwordEncoder.encode(password1));
                userRepository.save(user);
                return true;
            }
        }
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsername(username);
        if (user != null) {
            user.setDateOfLastVisit(new Date());
            userRepository.save(user);
        }
        return user;
    }
}
