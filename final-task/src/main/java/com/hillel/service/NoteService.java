package com.hillel.service;

import com.hillel.dto.NoteDto;
import com.hillel.model.Note;
import com.hillel.model.User;

import java.util.List;

public interface NoteService {

    void save(Note note);

    List<NoteDto> findAllByUser(User user);

    boolean update(Long id, String title, String text);

    void delete(Long id);
}
