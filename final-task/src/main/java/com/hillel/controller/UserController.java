package com.hillel.controller;

import com.hillel.dto.UserDto;
import com.hillel.model.User;
import com.hillel.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Log4j2
@AllArgsConstructor
@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/add")
    public String addNewUser(
            @RequestParam @Valid String username,
            @RequestParam @Valid String password,
            @RequestParam(defaultValue = "") String firstName,
            @RequestParam(defaultValue = "") String lastName,
            @RequestParam(defaultValue = "0") Integer age) {
        {
            User user = new User();
            user.setUsername(username);
            user.setPassword(password);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setAge(age);
            if (userService.save(user)) {
                log.info("User has been successfully registered");
            } else {
                log.info("The user has not been registered. " +
                        "Probably a user with username: '" + username + "' already exists");
            }
        }
        return "redirect:/login";
    }

    @GetMapping("/profile")
    public String profile(@AuthenticationPrincipal User user, Model model) {
        UserDto userDto = userService.findUserByUsername(user.getUsername());
        model.addAttribute("user", userDto);
        return "profile";
    }

    @PostMapping("/profile/change-password")
    public String changePassword(
            @RequestParam String newPassword,
            @RequestParam String confirmPassword,
            @AuthenticationPrincipal User user
    ) {
        if (userService.updatePassword(newPassword, confirmPassword, user)) {
            log.debug("User: '" + user.getUsername() + "' to set a new password");
        } else {
            log.debug("Password was not saved");
        }
        return "redirect:/user/profile";
    }

    @PostMapping("/profile/change-information")
    public String changeUserInformation(
            @RequestParam String username,
            @RequestParam(defaultValue = "") String firstName,
            @RequestParam(defaultValue = "") String lastName,
            @RequestParam(defaultValue = "0") Integer age,
            @AuthenticationPrincipal User user
    ) {
        if (userService.updateUserInformation(username, firstName, lastName, age, user)) {
            log.trace("User information updated");
        } else {
            log.trace("User information was not updated. Probably a user with the same name already exists");
        }
        return "redirect:/user/profile";
    }
}
