package com.hillel.controller;

import com.hillel.dto.NoteDto;
import com.hillel.model.Note;
import com.hillel.model.User;
import com.hillel.service.NoteService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Log4j2
@AllArgsConstructor
@Controller
@RequestMapping("/notes")
public class NoteController {

    private final NoteService noteService;

    @PostMapping("/add")
    public String addNewNote(
            @RequestParam String title,
            @RequestParam String text,
            @AuthenticationPrincipal User user
    ) {
        Note note = new Note();
        note.setTitle(title);
        note.setText(text);
        note.setUser(user);
        noteService.save(note);
        log.debug("New note created");
        return "redirect:/notes/";
    }

    @GetMapping
    public String getNotes(@AuthenticationPrincipal User user, Model model) {
        List<NoteDto> notes = noteService.findAllByUser(user);
        if (!notes.isEmpty()) {
            model.addAttribute("notes", notes);
            log.info("For user: '" + user.getUsername() + "' , found " + notes.size() + " note(s)");
        } else {
            log.info("User does not have any notes");
        }
        return "notes";
    }

    @PostMapping("/update")
    public String update(
            @RequestParam Long id,
            @RequestParam String title,
            @RequestParam String text
    ) {
        if (noteService.update(id, title, text)) {
            log.debug("Note updated");
        } else {
            log.debug("Note did not update");
        }
        return "redirect:/notes";
    }

    @PostMapping("/delete")
    public String delete(@RequestParam Long id) {
        noteService.delete(id);
        log.debug("Note deleted");
        return "redirect:/notes";
    }
}
