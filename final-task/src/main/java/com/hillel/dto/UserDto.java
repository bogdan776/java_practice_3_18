package com.hillel.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UserDto {

    private Long id;

    private String username;

    private String firstName;

    private String lastName;

    private Integer age;

    private Date dateOfRegistration;

    private Date dateOfLastVisit;

    private Boolean active;
}
