package com.hillel.dto;

import lombok.Data;

import java.util.Date;

@Data
public class NoteDto {

    private Long id;

    private String title;

    private String text;

    private Date dateOfCreation;

    private Date dateOfChange;

    public String getTextPreview() {
        if (text.length() >= 100) {
            return this.text.substring(0, 100);
        } else {
            return text;
        }
    }
}
