package com.hillel.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Entity
@Table(name = "notes", schema = "notes")
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "The field must be filled")
    @Length(max = 255, message = "Title too long")
    private String title;

    @NotBlank(message = "The field must be filled")
    @Length(max = 2048, message = "Text too long")
    private String text;

    private Date dateOfCreation;

    private Date dateOfChange;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
}
