package com.hillel.mapper;

import com.hillel.dto.UserDto;
import com.hillel.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(ignore = true, target = "active")
    UserDto userToUserDto(User user);
}
