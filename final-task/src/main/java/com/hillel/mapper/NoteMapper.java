package com.hillel.mapper;

import com.hillel.dto.NoteDto;
import com.hillel.model.Note;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NoteMapper {

    NoteDto noteToNoteDto(Note note);

    List<NoteDto> notesToNotesDto(List<Note> notes);
}
