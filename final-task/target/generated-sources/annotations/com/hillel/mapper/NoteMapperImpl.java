package com.hillel.mapper;

import com.hillel.dto.NoteDto;
import com.hillel.model.Note;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-05-29T07:56:10+0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_211 (Oracle Corporation)"
)
@Component
public class NoteMapperImpl implements NoteMapper {

    @Override
    public NoteDto noteToNoteDto(Note note) {
        if ( note == null ) {
            return null;
        }

        NoteDto noteDto = new NoteDto();

        noteDto.setId( note.getId() );
        noteDto.setTitle( note.getTitle() );
        noteDto.setText( note.getText() );
        noteDto.setDateOfCreation( note.getDateOfCreation() );
        noteDto.setDateOfChange( note.getDateOfChange() );

        return noteDto;
    }

    @Override
    public List<NoteDto> notesToNotesDto(List<Note> notes) {
        if ( notes == null ) {
            return null;
        }

        List<NoteDto> list = new ArrayList<NoteDto>( notes.size() );
        for ( Note note : notes ) {
            list.add( noteToNoteDto( note ) );
        }

        return list;
    }
}
