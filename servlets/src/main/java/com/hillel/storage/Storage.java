package com.hillel.storage;

import com.hillel.entity.Status;
import com.hillel.entity.User;
import com.hillel.exception.Messages;
import com.hillel.exception.StorageException;
import com.hillel.exception.UserNotFoundException;
import com.hillel.web.constant.ParametersNames;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Storage {

    private List<User> users;


    public Storage() {
        this.users = new ArrayList<>();
    }

    public void addUserToList(User user) throws StorageException {
        if (notContainUser(user.getUsername())) {
            users.add(user);
        } else throw new StorageException(Messages.ERR_USER_ALREADY_EXISTS);
    }

    public boolean logIn(String username, String password) throws StorageException {
        try {
            users.stream()
                    .filter(i -> i.getUsername().equals(username) && i.getPassword().equals(password))
                    .findFirst().orElseThrow(UserNotFoundException::new)
                    .setStatus(Status.LOGGED_IN);
            return true;
        } catch (UserNotFoundException ex) {
            throw new StorageException(Messages.ERR_USER_NOT_FOUND);
        }
    }

    public User findUserByUsername(String username) throws StorageException {
        try {
            return users.stream().filter(i -> i.getUsername().equals(username))
                    .findFirst().orElseThrow(UserNotFoundException::new);
        } catch (UserNotFoundException e) {
            throw new StorageException(Messages.ERR_USER_NOT_FOUND);
        }
    }

    public boolean deleteUserFromList(String username) throws StorageException {
        if (users.removeIf(user -> user.getUsername().equals(username))) {
            return true;
        } else throw new StorageException(Messages.ERR_USER_NOT_FOUND);
    }

    public void logOut(String username) throws StorageException {
        findUserByUsername(username).setStatus(Status.NOT_LOGGED_IN);
    }

    public void executeUpdate(String parameter, String username, String value) throws StorageException {
        switch (parameter) {
            case ParametersNames.FIRST_NAME:
                updateFirstName(username, value);
                break;
            case ParametersNames.LAST_NAME:
                updateLastName(username, value);
                break;
            case ParametersNames.USERNAME:
                updateUsername(username, value);
                break;
        }
    }

    private void updateFirstName(String username, String firsName) throws StorageException {
        findUserByUsername(username).setFirstName(firsName);
    }

    private void updateLastName(String username, String lastName) throws StorageException {
        findUserByUsername(username).setLastName(lastName);
    }

    private void updateUsername(String username, String newValue) throws StorageException {
        findUserByUsername(username).setUsername(newValue);
    }

    public boolean notContainUser(String username) {
        return users.stream().noneMatch(i -> i.getUsername().equals(username));
    }
}
