package com.hillel.entity;

public enum Status {

    LOGGED_IN, NOT_LOGGED_IN
}
