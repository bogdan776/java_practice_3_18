package com.hillel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Note {

    private int id;
    private String title;
    private boolean completed;
}
