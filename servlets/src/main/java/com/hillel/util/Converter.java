package com.hillel.util;

import com.hillel.dto.UserDTO;
import com.hillel.entity.User;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    private Converter() {
        /*
        No
         */
    }

    public static UserDTO convertUserToUserDTO(User user) {
        return new UserDTO(user.getFirstName(), user.getLastName(), user.getUsername(), user.getStatus());
    }

    public static List<UserDTO> convertUsersToUsersDTO(List<User> users) {
        List<UserDTO> usersDTO = new ArrayList<>();
        for (User u : users) {
            usersDTO.add(convertUserToUserDTO(u));
        }
        return usersDTO;
    }
}
