package com.hillel.util;

import com.hillel.entity.User;
import com.hillel.exception.Messages;
import com.hillel.exception.ValidationException;
import com.hillel.web.constant.ParametersNames;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    private static final String REGEX_USERNAME = "(?i)^[a-z0-9_-]{3,16}$";

    private static final String REGEX_PASSWORD = "^(?=.*[A-Z])(?=.*[!#$&_])(?=.*[0-9])" +
            "(?=.*[a-z].*[a-z].*[a-z]).{8,}$";

    private static final String TAKE_IT_EASY = "^\\w+$";

    public static String validateByParameter(String parameter, String value) throws ValidationException {
        switch (parameter) {
            case ParametersNames.USERNAME:
                return validateUsername(value);
            case ParametersNames.FIRST_NAME:
            case ParametersNames.LAST_NAME:
                return validate(value);
            case ParametersNames.PASSWORD:
                return validatePassword(value);
            default:
                throw new ValidationException(Messages.ERR_CANNOT_VALIDATE_PARAMETER + parameter);
        }
    }

    public static User validate(User user) throws ValidationException {
        User newUser = new User();
        newUser.setFirstName(validate(user.getFirstName()));
        newUser.setLastName(user.getLastName());
        newUser.setUsername(validateUsername(user.getUsername()));
        newUser.setPassword(validatePassword(user.getPassword()));
        return newUser;
    }

    private static String validateUsername(String username) throws ValidationException {
        try {
            return doValidation(REGEX_USERNAME, username);
        } catch (ValidationException e) {
            throw new ValidationException(Messages.ERR_CANNOT_VALIDATE_USERNAME);
        }
    }

    private static String validatePassword(String password) throws ValidationException {
        try {
            return doValidation(REGEX_PASSWORD, password);
        } catch (ValidationException e) {
            throw new ValidationException(Messages.ERR_CANNOT_VALIDATE_PASSWORD);
        }
    }

    private static String validate(String value) throws ValidationException {
        return doValidation(TAKE_IT_EASY, value);
    }

    private static String doValidation(String regex, String value) throws ValidationException {
        if (value.isEmpty()) {
            throw new ValidationException(Messages.EMPTY_FIELD);
        }
        Matcher matcher = Pattern.compile(regex).matcher(value);
        if (matcher.find()) {
            String result = matcher.group();
            if (value.equals(result)) {
                return result;
            } else throw new ValidationException(Messages.ERR_INVALID_INPUT);
        } else throw new ValidationException(Messages.ERR_CANNOT_VALIDATE_VALUE + value);
    }
}
