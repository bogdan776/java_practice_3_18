package com.hillel.exception;

public class AppException extends Exception {

    public AppException() {
        super();
    }

    public AppException(String message) {
        super(message);
    }
}
