package com.hillel.exception;

public final class Messages {

    private Messages() {
        /*
        cannot get class instance
         */
    }

    /*
    For validator
     */
    public static final String ERR_CANNOT_VALIDATE_PARAMETER = "Cannot validate parameter: ";

    public static final String ERR_CANNOT_VALIDATE_VALUE = "Cannot validate value: ";

    public static final String ERR_INVALID_INPUT = "Cannot validate input field. Invalid input";

    public static final String ERR_CANNOT_VALIDATE_PASSWORD = "Cannot validate password";

    public static final String ERR_CANNOT_VALIDATE_USERNAME = "Cannot validate username";

    public static final String EMPTY_FIELD = "Empty field";

    /*
    Other messages
     */
    public static final String ERR_USER_NOT_FOUND = "User not found";

    public static final String ERR_USER_ALREADY_EXISTS = "A user with the same username already exists";
}
