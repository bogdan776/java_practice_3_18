package com.hillel.exception;

public class StorageException extends AppException {

    public StorageException() {
        super();
    }

    public StorageException(String message) {
        super(message);
    }
}
