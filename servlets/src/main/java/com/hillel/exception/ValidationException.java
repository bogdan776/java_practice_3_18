package com.hillel.exception;

public class ValidationException extends AppException {

    public ValidationException() {
        super();
    }

    public ValidationException(String message) {
        super(message);
    }
}
