package com.hillel.service;

import com.hillel.entity.Status;
import com.hillel.entity.User;
import com.hillel.exception.StorageException;
import com.hillel.exception.ValidationException;
import com.hillel.storage.Storage;
import com.hillel.util.Validator;
import com.hillel.web.constant.ParametersNames;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Map;

public class UserService {

    private Storage storage;

    public UserService(Storage storage) {
        this.storage = storage;
        init();
    }

    public boolean delete(String username) throws StorageException {
        if (username == null) {
            return false;
        }
        return storage.deleteUserFromList(username);
    }

    public boolean updateUserInfo(String username, Map<String, String> parameters) throws StorageException {
        if (parameters.containsKey(ParametersNames.USERNAME)) {
            String newUsername = parameters.get(ParametersNames.USERNAME);
            if (!checkUsername(username, newUsername)) {
                return false;
            }
        }
        update(parameters, username);
        return true;
    }

    public void signUp(User user) throws ValidationException, StorageException {
        User newUser = Validator.validate(user);
        String sha256 = DigestUtils.sha256Hex(newUser.getPassword());
        newUser.setPassword(sha256);
        newUser.setStatus(Status.NOT_LOGGED_IN);
        storage.addUserToList(newUser);
    }

    public boolean logIn(String username, String password) throws StorageException {
        if (username == null || password == null) {
            return false;
        }
        String sha256Password = DigestUtils.sha256Hex(password);
        return storage.logIn(username, sha256Password);
    }

    public void logOut(String username) throws StorageException {
        storage.logOut(username);
    }

    public User getUserByUsername(String username) throws StorageException {
        return storage.findUserByUsername(username);
    }

    public Storage getAllUsers() {
        return storage;
    }

    private boolean checkUsername(String originalUsername, String newUsername) {
        if (newUsername != null && !newUsername.equals(originalUsername)) {
            return storage.notContainUser(newUsername);
        } else {
            return false;
        }
    }

    private void update(Map<String, String> params, String username) throws StorageException {
        String parameter;
        String value;
        for (Map.Entry entry : params.entrySet()) {
            parameter = String.valueOf(entry.getKey());
            value = String.valueOf(entry.getValue());
            storage.executeUpdate(parameter, username, value);
        }
    }

    private void init() {
        for (int i = 0; i < 5; i++) {
            try {
                signUp(new User("Name" + i,
                        "Surname" + i, "username" + i,
                        "PASS$word123" + i, Status.NOT_LOGGED_IN));
            } catch (ValidationException | StorageException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
