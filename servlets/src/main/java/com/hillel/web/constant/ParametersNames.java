package com.hillel.web.constant;

public final class ParametersNames {

    private ParametersNames() {
        /*
        cannot get class instance
         */
    }

    public static final String USER_SERVICE = "userService";

    public static final String USER = "user";

    public static final String USERNAME = "username";

    public static final String FIRST_NAME = "firstName";

    public static final String LAST_NAME = "lastName";

    public static final String PASSWORD = "password";
}
