package com.hillel.web.constant;

public final class Paths {

    private Paths() {
        /*
        cannot get class instance
         */
    }

    /*
    Path to JSP pages
     */

    public static final String LOGIN_PAGE = "/WEB-INF/jsp/log_in_page.jsp";

    public static final String SUCCESS_PAGE = "/WEB-INF/jsp/success_page.jsp";

    public static final String LOG_OUT_PAGE = "/WEB-INF/jsp/log_out_page.jsp";

    public static final String INDEX = "/index.jsp";

    public static final String SIGN_UP_PAGE = "/WEB-INF/jsp/sign_up_page.jsp";

    /*
    Path to Servlet
     */

    public static final String LOG_IN = "/user/login";
}
