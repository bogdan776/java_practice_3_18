package com.hillel.web.filter;

import com.hillel.web.constant.ParametersNames;
import com.hillel.web.constant.Paths;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Log4j
@WebFilter(urlPatterns = {"/user/logout", "/user/login/success"})
public class UserFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
        log.debug("Init");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        if (session.getAttribute(ParametersNames.USER) == null) {
            log.info("Unauthorized user");
            resp.sendRedirect(req.getContextPath() + Paths.LOG_IN);
        } else chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        log.debug("Destroyed");
    }
}
