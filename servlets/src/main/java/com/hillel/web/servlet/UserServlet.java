package com.hillel.web.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.dto.UserDTO;
import com.hillel.entity.User;
import com.hillel.exception.StorageException;
import com.hillel.exception.ValidationException;
import com.hillel.service.UserService;
import com.hillel.util.Converter;
import com.hillel.util.Validator;
import com.hillel.web.constant.Paths;
import lombok.extern.log4j.Log4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import static com.hillel.web.constant.ParametersNames.*;

@Log4j
@WebServlet("/user")
public class UserServlet extends HttpServlet {

    private UserService userService = null;

    @Override
    public void init() {
        log.debug("Servlet initialization started");
        userService = (UserService) getServletContext().getAttribute(USER_SERVICE);
        log.debug("Servlet initialization finished");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = request.getParameter(USERNAME);
        if (username != null) {
            try {
                User user = userService.getUserByUsername(username);
                UserDTO userDTO = Converter.convertUserToUserDTO(user);
                String json = new ObjectMapper().writeValueAsString(userDTO);
                response.getWriter().println(json);
            } catch (StorageException e) {
                log.error(e.getMessage());
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "User with login: " + username + " not found");
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid parameter: " +
                    request.getParameterNames().nextElement());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String json = reader.readLine();
        ObjectMapper mapper = new ObjectMapper();
        User user = mapper.readValue(json, User.class);
        reader.close();
        try {
            userService.signUp(user);
            log.trace("User created");
            response.setStatus(HttpServletResponse.SC_CREATED);
        } catch (ValidationException | StorageException e) {
            log.error(e.getMessage());
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getSession().getAttribute(USER) != null) {
            User user = (User) request.getSession().getAttribute(USER);
            try {
                if (userService.updateUserInfo(user.getUsername(), getParametersFromRequest(request))) {
                    log.trace("User updated");
                    response.setStatus(HttpServletResponse.SC_OK);
                } else {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            } catch (StorageException e) {
                log.error(e.getMessage());
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
            } catch (ValidationException e) {
                log.error(e.getMessage());
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else response.sendRedirect(getServletContext().getContextPath() + Paths.LOG_IN);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            if (userService.delete(request.getParameter(USERNAME))) {
                log.error("User deleted");
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (StorageException e) {
            log.error(e.getMessage());
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private Map<String, String> getParametersFromRequest(HttpServletRequest request) throws ValidationException {
        Enumeration<String> enumeration = request.getParameterNames();
        Map<String, String> parameters = new HashMap<>();
        String paramName;
        while (enumeration.hasMoreElements()) {
            paramName = enumeration.nextElement();
            parameters.put(paramName, Validator.validateByParameter(paramName, request.getParameter(paramName)));
        }
        return parameters;
    }
}
