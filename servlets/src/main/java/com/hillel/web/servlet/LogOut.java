package com.hillel.web.servlet;

import com.hillel.entity.User;
import com.hillel.exception.StorageException;
import com.hillel.service.UserService;
import com.hillel.web.constant.ParametersNames;
import com.hillel.web.constant.Paths;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.hillel.web.constant.ParametersNames.USER_SERVICE;

@Log4j
@WebServlet("/user/logout")
public class LogOut extends HttpServlet {

    private UserService userService = null;

    private static final String ANSWER = "answer";

    @Override
    public void init() {
        log.debug("Servlet initialization started");
        userService = (UserService) getServletContext().getAttribute(USER_SERVICE);
        log.debug("Servlet initialization finished");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(Paths.LOG_OUT_PAGE).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!request.getParameter(ANSWER).isEmpty()) {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(ParametersNames.USER);
            session.invalidate();
            try {
                userService.logOut(user.getUsername());
                response.sendRedirect(getServletContext().getContextPath() + Paths.LOG_IN);
            } catch (StorageException e) {
                log.error(e.getMessage());
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        } else request.getRequestDispatcher(Paths.INDEX).forward(request, response);
    }
}
