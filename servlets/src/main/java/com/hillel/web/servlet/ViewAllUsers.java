package com.hillel.web.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.dto.UserDTO;
import com.hillel.service.UserService;
import com.hillel.storage.Storage;
import com.hillel.util.Converter;
import com.hillel.web.constant.ParametersNames;
import lombok.extern.log4j.Log4j;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Log4j
@WebServlet("/users")
public class ViewAllUsers extends HttpServlet {

    private UserService userService = null;

    @Override
    public void init() {
        log.debug("Servlet initialization started");
        userService = (UserService) getServletContext().getAttribute(ParametersNames.USER_SERVICE);
        log.debug("Servlet initialization finished");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Storage storage = userService.getAllUsers();
        if (storage != null) {
            List<UserDTO> users = Converter.convertUsersToUsersDTO(storage.getUsers());
            PrintWriter writer = response.getWriter();
            writer.print(new ObjectMapper().writeValueAsString(users));
            writer.close();
        } else {
            log.error("Users not found");
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
