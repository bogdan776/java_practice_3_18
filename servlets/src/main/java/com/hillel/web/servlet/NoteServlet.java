package com.hillel.web.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.entity.Note;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/notes")
public class NoteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<Note> notes = new ArrayList<>();
        notes.add(new Note(1, "One", false));
        notes.add(new Note(2, "Two", false));
        notes.add(new Note(3, "Three", true));
        notes.add(new Note(4, "Four", false));
        PrintWriter writer = resp.getWriter();
        writer.print(new ObjectMapper().writeValueAsString(notes));
        writer.close();

    }
}
