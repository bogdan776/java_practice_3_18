package com.hillel.web.servlet;


import com.hillel.exception.StorageException;
import com.hillel.service.UserService;
import com.hillel.web.constant.Paths;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.hillel.web.constant.ParametersNames.*;

@Log4j
@WebServlet("/user/login")
public class LogIn extends HttpServlet {

    private UserService userService = null;

    @Override
    public void init() {
        log.debug("Servlet initialization started");
        userService = (UserService) getServletContext().getAttribute(USER_SERVICE);
        log.debug("Servlet initialization finished");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.trace("Forward to login page");
        request.getRequestDispatcher(Paths.LOGIN_PAGE).forward(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = request.getParameter(USERNAME);
        String password = request.getParameter(PASSWORD);
        try {
            if (userService.logIn(username, password)) {
                log.trace("Authorization was successful");
                request.getSession().setAttribute(USER, userService.getUserByUsername(username));
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                log.trace("Authorization failed");
            }
        } catch (StorageException e) {
            log.error(e.getMessage());
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
