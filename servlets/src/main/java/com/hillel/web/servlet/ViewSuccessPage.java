package com.hillel.web.servlet;

import com.hillel.web.constant.Paths;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
@WebServlet("/user/login/success")
public class ViewSuccessPage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.trace("Forward to success page");
        request.getRequestDispatcher(Paths.SUCCESS_PAGE).forward(request, response);
    }
}
