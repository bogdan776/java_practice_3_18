package com.hillel.web.listener;

import com.hillel.service.UserService;
import com.hillel.storage.Storage;
import com.hillel.web.constant.ParametersNames;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@Log4j
@WebListener
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        log.debug("Servlet context initialization started");
        Storage storage = new Storage();
        UserService userService = new UserService(storage);
        ServletContext servletContext = event.getServletContext();
        servletContext.setAttribute(ParametersNames.USER_SERVICE, userService);
        log.debug("Servlet context initialization finished");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.debug("Destroyed");
    }
}
