<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log out</title>
</head>
<body>
<p>Are you sure?</p>
<form action="<c:url value="/user/logout"/>" method="post">
    <button value="yes" name="answer">Yes</button>
    <button value="" name="answer">No</button>
</form>
</body>
</html>
