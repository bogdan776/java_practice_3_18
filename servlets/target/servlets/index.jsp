<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
<h2>World</h2>
<c:if test="${not empty sessionScope.user}">
    <p><i><a href="<c:url value="/user/logout"/>">Log out</a></i></p>
</c:if>
<c:if test="${empty sessionScope.user}">
    <p><i><a href="<c:url value="/user/login"/>">Log in</a></i></p>
</c:if>
</body>
</html>