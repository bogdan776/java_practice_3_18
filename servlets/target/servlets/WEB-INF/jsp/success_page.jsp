<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Success</title>
</head>
<body>
<h1>Welcome!</h1>
<p><i>Do you want to sign out?<a href="<c:url value="/user/logout"/>"> click on!</a></i></p>
</body>
</html>