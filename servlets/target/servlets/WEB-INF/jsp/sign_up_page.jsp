<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign up</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
<form>
    <p>First name
        <input name="firstName" id="firstName" placeholder="First name"></p>
    <p>Last name
        <input name="lastName" id="lastName" placeholder="Last name"></p>
    <p>Username
        <input name="username" id="username" placeholder="username"></p>
    <p>Password
        <input type="password" name="password" id="password" placeholder="Password"></p>
    <p><input type="submit" id="button-signUp" value="Go"></p>
</form>
</body>
<script>
    $(document).ready(function () {
        var user = {};
        $('#button-signUp').click(function (event) {
            event.preventDefault();
            user.firstName = $('#firstName').val();
            user.lastName = $('#lastName').val();
            user.username = $('#username').val();
            user.password = $('#password').val();
            const path = '${pageContext.servletContext.contextPath}/user';
            const URL = path + '?' +
                'firstName=' + user.firstName + '&' +
                'lastName=' + user.lastName + '&' +
                'username=' + user.username + '&' +
                'password=' + user.password;

            const json = JSON.stringify(user);
            $.ajax({
                url: URL,
                method: 'POST',
                data: json,
                contentType: 'application/json; charset=utf-8',
                complete: function (response) {
                    if (response.status === 201) {
                        top.location.href = path + '/login';
                    } else {
                        alert(response.status + ' Bad Request\nUser not created! Try again');
                        top.location.href = path + '/signUp';
                    }
                }
            });
        });
    });
</script>
</html>