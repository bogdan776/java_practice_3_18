<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="resources/css/main.css" type="text/css"/>
</head>
<body>
<form action="<c:url value="/user"/>" method="get">
    <input type="text" name="username" placeholder="Enter username">
    <input type="submit" value="Find">
</form>
<div id="user_table"></div>
</body>
<script>
    var userJson = ${requestScope.user};
    $(document).ready(function () {
        var table = $('<table/>').appendTo($('#user_table'));
        $('<tr/>').appendTo(table)
            .append($('<th/>').text('First Name'))
            .append($('<th/>').text('Last name'))
            .append($('<th/>').text('Username'))
            .append($('<th/>').text('Status'));
        $('<tr/>').appendTo(table)
            .append($('<td/>').text(userJson.firstName))
            .append($('<td/>').text(userJson.lastName))
            .append($('<td/>').text(userJson.username))
            .append($('<td/>').text(userJson.status));
    });
</script>
</html>
