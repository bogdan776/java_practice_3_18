<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Log in</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
<form>
    <p>Username
        <input name="username" id="username" placeholder="Username"/></p>
    <p>Password
        <input name="password" id="password" type="password" placeholder="Password"/></p>
    <input type="submit" id="button-login" value="Log in">
    <p><i>Do not have an account?<a href="<c:url value="/user/signUp"/>"> click on!</a></i></p>
</form>
</body>
<script>
    $(document).ready(function () {
        $('#button-login').click(function (event) {
            event.preventDefault();
            const username = $('#username').val();
            const password = $('#password').val();
            const path = '${pageContext.servletContext.contextPath}/user/login';
            const URL = path + '?' +
                'username=' + username + '&' +
                'password=' + password;
            $.ajax({
                url: URL,
                method: 'PUT',
                complete: function (response) {
                    if (response.status === 200) {
                        top.location.href = path + '/success';
                    } else if (response.status === 404) {
                        alert(response.status + '\nIncorrect login or password');
                        top.location.href = path;
                    } else {
                        alert(response.status + ' Bad Request! Try again');
                    }
                }
            });
        });
    });
</script>
</html>