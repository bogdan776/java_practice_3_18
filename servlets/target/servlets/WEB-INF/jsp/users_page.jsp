<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        table {
            font-family: "Calibri Light", monospace;
            border-collapse: collapse;
            width: 40%;
        }

        td, th {
            text-align: center;
            padding: 10px;
        }

        tr:hover {
            background-color: whitesmoke;
        }

        th {
            background-color: lightgrey;
            color: black;
        }
    </style>
</head>
<body>
<h3>Users</h3>
<div id="user_table"></div>
</body>
<script>
    var usersJson = ${requestScope.users};
    $(document).ready(function () {
        var table = $('<table/>').appendTo($('#user_table'));
        $('<tr/>').appendTo(table)
            .append($('<th/>').text('First Name'))
            .append($('<th/>').text('Last name'))
            .append($('<th/>').text('Username'))
            .append($('<th/>').text('Status'));
        $(usersJson).each(function (i, user) {
            $('<tr/>').appendTo(table)
                .append($('<td/>').text(user.firstName))
                .append($('<td/>').text(user.lastName))
                .append($('<td/>').text(user.username))
                .append($('<td/>').text(user.status));
        });
    });
</script>
</html>