package com.hillel.service;

import com.hillel.db.UserDB;
import com.hillel.entity.User;
import com.hillel.impl.UserDBImpl;

import java.util.List;

public class UserService {

    private UserDB userDB = new UserDBImpl();

    public void signUp(String login, String password, String name, String phoneNumber) {
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setName(name);
        user.setPhoneNumber(phoneNumber);
        userDB.create(user);
    }

    public User logIn(String login, String password) {
        return userDB.logIn(login, password);
    }

    public List<User> getAllUsers() {
        return userDB.getAll();
    }

    public User getById(int id) {
        return userDB.getById(id);
    }

    public void deleteUser(int id) {
        userDB.deleteById(id);
    }
}