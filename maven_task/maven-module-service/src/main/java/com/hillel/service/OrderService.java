package com.hillel.service;

import com.hillel.db.OrderDB;
import com.hillel.entity.Order;
import com.hillel.impl.OrderDBImpl;

import java.util.List;

public class OrderService {

    private OrderDB orderDB = new OrderDBImpl();

    public void addOrderToDB(int userId, String orderDate, String title) {
        Order order = new Order();
        order.setUserId(userId);
        order.setOrderDate(orderDate);
        order.setTitle(title);
        orderDB.create(order);
    }

    public List<Order> getAllOrders() {
        return orderDB.getAll();
    }

    public Order getByID(int id) {
        return orderDB.getById(id);
    }

    public void delete(int id) {
        orderDB.deleteById(id);
    }

    public List<Order> getOrdersByUserId(int id) {
        return orderDB.getOrdersByUserId(id);
    }
}