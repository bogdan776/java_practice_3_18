package com.hillel.impl;

import com.hillel.db.OrderDB;
import com.hillel.entity.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderDBImpl implements OrderDB {

    private List<Order> orders;

    {
        orders = new ArrayList<>();

        Order o1 = new Order();
        o1.setId(0);
        o1.setUserId(0);
        o1.setOrderDate("2019-01-01");
        o1.setTitle("Order 1");

        Order o2 = new Order();
        o2.setId(1);
        o2.setUserId(1);
        o2.setOrderDate("2019-01-02");
        o2.setTitle("Order 2");

        orders.add(o1);
        orders.add(o2);
    }

    @Override
    public void create(Order order) {
        Order newOrder = order;
        newOrder.setId(orders.size());
        orders.add(order);
    }

    @Override
    public List<Order> getAll() {
        return new ArrayList<>(orders);
    }

    @Override
    public Order getById(int id) {
        return orders.get(id);
    }

    @Override
    public void deleteById(int id) {
        orders.remove(id);
    }

    @Override
    public List<Order> getOrdersByUserId(int id) {
        return orders.stream().filter(a -> a.getUserId() == id).collect(Collectors.toList());
    }
}