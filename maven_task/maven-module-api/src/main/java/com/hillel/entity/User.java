package com.hillel.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class User extends Entity {

    private String login;
    private String password;
    private String name;
    private String phoneNumber;
}