package com.hillel.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Order extends Entity {

    private int userId;
    private String orderDate;
    private String title;
}