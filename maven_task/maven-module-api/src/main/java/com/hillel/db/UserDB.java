package com.hillel.db;

import com.hillel.entity.User;

public interface UserDB extends EntityDB<User> {

    User logIn(String login, String password);
}