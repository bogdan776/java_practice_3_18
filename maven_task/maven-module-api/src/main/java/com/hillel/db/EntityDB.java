package com.hillel.db;

import com.hillel.entity.Entity;

import java.util.List;

public interface EntityDB<T extends Entity> {

    void create(T entity);

    List<T> getAll();

    T getById(int id);

    void deleteById(int id);
}